#include <gtk/gtk.h>
#include <gtksourceview/gtksource.h>

#include "gb-source-fullscreen-container.h"

gint
main (gint   argc,
      gchar *argv[])
{
   GtkSourceStyleSchemeManager *sm;
   GtkSourceLanguageManager *lm;
   GtkSourceStyleScheme *s;
   PangoFontDescription *font;
   GtkSourceLanguage *l;
   GtkSourceBuffer *buffer;
   GtkWidget *window;
   GtkWidget *source_view;
   GtkWidget *container;

   gtk_init(&argc, &argv);

   window = g_object_new(GTK_TYPE_WINDOW,
                         "title", "Source View",
                         NULL);
   g_signal_connect(window, "delete-event", gtk_main_quit, NULL);

   container = g_object_new(GB_TYPE_SOURCE_FULLSCREEN_CONTAINER,
                            "visible", TRUE,
                            NULL);
   gtk_container_add(GTK_CONTAINER(window), container);

   lm = gtk_source_language_manager_get_default();
   l = gtk_source_language_manager_get_language(lm, "c");
   sm = gtk_source_style_scheme_manager_get_default();
   gtk_source_style_scheme_manager_append_search_path(sm, ".");
   s = gtk_source_style_scheme_manager_get_scheme(sm, "solarized-light");
   if (g_getenv("STYLE_SCHEME"))
      s = gtk_source_style_scheme_manager_get_scheme(sm, g_getenv("STYLE_SCHEME"));
   else
      s = gtk_source_style_scheme_manager_get_scheme(sm, "tango");
   buffer = g_object_new(GTK_SOURCE_TYPE_BUFFER,
                         "language", l,
                         "style-scheme", s,
                         NULL);

   source_view = g_object_new(GTK_SOURCE_TYPE_VIEW,
                              "buffer", buffer,
                              "right-margin-position", 80,
                              "show-line-numbers", TRUE,
                              "show-right-margin", TRUE,
                              "visible", TRUE,
                              NULL);
   gtk_container_add(GTK_CONTAINER(container), source_view);

   font = pango_font_description_from_string("Monospace");
   gtk_widget_override_font(source_view, font);
   pango_font_description_free(font);

   gtk_window_present(GTK_WINDOW(window));
   gtk_window_fullscreen(GTK_WINDOW(window));
   gtk_main();

   return 0;
}
