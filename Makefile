all: spacer

spacer: spacer.c gb-source-fullscreen-container.c gb-source-fullscreen-container.h
	$(CC) -g -o $@ -Wall -Werror spacer.c gb-source-fullscreen-container.c $(shell pkg-config --cflags --libs gtksourceview-3.0)
